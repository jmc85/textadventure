﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class InputController : MonoBehaviour
{
    public InputField inputField;
    public Toggle mode;
    public Text displayText; //reference to large text area
    public List<string> valid = new List<string>(); //our valid commands
    public Image background;
    public Text toggleLabel;
    public Text placeHolder;
    public Text inputFieldText;
    public Button increase, decrease;

    private string story;
    private bool darkMode; //if true - darkmode is on; if false darkmode is off

    public delegate void GameOver(); //set up the delegate and template
    public event GameOver onGameOver; //create event linked to delegate
 
    void Start()
    {
        //UpdateDisplayText("You wake up in a dark room."); //added
        inputField.onEndEdit.AddListener(GetInput);
        mode.onValueChanged.AddListener(ModeChange);
        valid.Add("go");  //valid commands
        valid.Add("get");
        valid.Add("restart");
        valid.Add("save");
        
        valid.Add("inventory");
        valid.Add("commands");

        increase.onClick.AddListener(IncreaseFont);
        decrease.onClick.AddListener(DecreaseFont);

        if(PlayerPrefs.HasKey("fontSize"))
        {
            displayText.fontSize = PlayerPrefs.GetInt("fontSize");
        }
        else
        {
            PlayerPrefs.SetInt("fontSize", displayText.fontSize);
            PlayerPrefs.Save();
        }

        //load saved preferences correctly
        if (PlayerPrefs.HasKey("DarkMode"))
        {
            int isDark = PlayerPrefs.GetInt("DarkMode");
            darkMode = isDark == 1;
            SetTheme();
        }
        else //key not found
        {
            PlayerPrefs.SetInt("DarkMode", 1); //by default the dark mode is enabled
            PlayerPrefs.Save();
        }
    }

    void IncreaseFont()
    {
        displayText.fontSize += 1;
        PlayerPrefs.SetInt("fontSize", displayText.fontSize);
        PlayerPrefs.Save();
    }

    void DecreaseFont()
    {
        displayText.fontSize -= 1;
        PlayerPrefs.SetInt("fontSize", displayText.fontSize);
        PlayerPrefs.Save();
    }

    //notifies of change in preference
    void ModeChange(bool userInput)
    {
        //player pref
        darkMode = userInput;
        PlayerPrefs.SetInt("DarkMode", darkMode ? 1 : 0);
        PlayerPrefs.Save();
        SetTheme();
    }

    void SetTheme()
    {
        if (darkMode)
        {
            background.color = Color.black;
            displayText.color = Color.white;
            toggleLabel.color = Color.white;
            placeHolder.color = Color.white;
            inputFieldText.color = Color.white;
            mode.isOn = true;
        }
        else
        {
            background.color = Color.white;
            displayText.color = Color.black;
            toggleLabel.color = Color.black;
            placeHolder.color = Color.black;
            inputFieldText.color = Color.black;
            mode.isOn = false;
        }
    }

    void GetInput(string userInput)
    {

        inputField.text = "";  // clear user input
        inputField.ActivateInputField(); // put user back in input field (so they don't have to click!)

        if(userInput != "") //not empty
        {
            char[] splitInfo = { ' ' }; //delimeter
            string[] inputCommands = userInput.Trim().ToLower().Split(splitInfo); //break user input up
            if (valid.Contains(inputCommands[0]))
            { //is this a valid command
                Debug.Log(userInput + " found");
                UpdateDisplayText(userInput);
                if (inputCommands[0] == "go")
                {
                    bool result = GameManager.instance.nav.SwitchRoom(inputCommands[1]);

                    if (result) //if true - switched to a valid room
                    {
                        UpdateDisplayText(GameManager.instance.nav.Unpack());
                        if (GameManager.instance.nav.GetNumExits() == 0)
                        {
                            UpdateDisplayText("Type 'Restart' to Restart");
                            if(onGameOver != null) //are any functions connected -- is anyone listening?
                                onGameOver();
                        }
                    }
                    else
                        UpdateDisplayText("Sorry, there is no exit in that direction or the door is locked");
                }
                else if (inputCommands[0] == "restart") //lowercase!!!
                    SceneManager.LoadScene(0);
                else if(inputCommands[0] == "get")
                {
                    // get the key and add it to inventory
                    if (GameManager.instance.nav.TakeItem(inputCommands[1]))
                    {
                        //add to inventory
                        GameManager.instance.inventory.Add(inputCommands[1]);
                        UpdateDisplayText(inputCommands[1] + " item has been added to the inventory");
                    }
                    else
                        UpdateDisplayText("That item is not in this room. Please try again");
                }
                else if(inputCommands[0] == "save")
                {
                    //call to save function
                    GameManager.instance.Save();
                }
                //UpdateDisplayText(userInput); //moved this up

                //Display List of Commands
                else if (inputCommands[0] == "commands")
                {
                    UpdateDisplayText("The Valid Commands Are:");
                    for (int i = 0; i < valid.Count - 1; i++)
                    {
                        UpdateDisplayText(valid[i]);
                    }
                }

                //Display Contents of Inventory
                else if (inputCommands[0] == "inventory")
                {
                    GameManager.instance.showInventory();
                }
            }
            else
            {
                Debug.Log(userInput + " not found");
                UpdateDisplayText("Command not found");
            }
        }

        //Debug.Log(userInput);
    } //end of GetInput function

    public void UpdateDisplayText(string msg)
    {
        story += "\n" + msg;
        displayText.text = story;
    }
}
