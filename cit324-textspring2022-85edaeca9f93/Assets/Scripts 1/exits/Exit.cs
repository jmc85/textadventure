﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName ="TextAdventure/Exit")]
public class Exit : ScriptableObject
{
    public enum Direction { north, south, east, west};
    public Direction theDirection;
    [TextArea]
    public string description;
    public Room room; //the room the exit is attached to
    public bool locked = false;
}
